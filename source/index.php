<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
    <header>
        <h1>
            <?= "Welcome to my website!" ?>
        </h1>
    </header>
    <div class="content">
        <section class="lorems">
            <article>
                <h3 class="article-title">First article</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.
                </p>
            </article>
            <article>
                <h3 class="article-title">Second article</h3>
                <p>
                    Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </article>
        </section>
    </div>
    <footer>
        <p>The current year is: 2018</p>
    </footer>
</body>
</html>
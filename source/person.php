<?php
    // require_once __DIR__ . "/Person.php";
    
    class Ball
    {
        public $color;
    }

?>
<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>Person</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
    <header>
        <h1>
            Person
        </h1>
    </header>
    <div class="content">
        <pre>
        <?php
            /*
            $p = new Person("Mario", 30);
            
            echo $p->name . "\n";
            echo $p->age . "\n";
            */
            $ball = new Ball();
            $ball->color = "red";
            
            var_dump($ball);
        ?>
        </pre>
    </div>
</body>
</html>

class Person
{
    private $name;
    private $age;
    
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function setName($val)
    {
        $this->name = $val;
    }

    public function setAge($val)
    {
        $this->age = $val;
    }


    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }
}